def sumaTodos(limitTo, f):
    resultado = 0

    for i in range(limitTo + 1):
        resultado += f(i)

    return resultado


# print(sumaTodos(3, lambda x: x * 2)) # 0*2 + 1*2 + 2*2 + 3*2

print(sumaTodos(100, lambda x: x**2))
