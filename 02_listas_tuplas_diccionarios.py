
# LISTAS

fruits = ["Apple", "Orange", "Watermelon", "Lemon"]

cosas = [1, 3 == 4, 'yo', fruits]

# print(cosas)

manzana = fruits[0]
limon = fruits[-1]

numero_frutas = len(fruits)

# print(manzana, limon, numero_frutas)

posicion_naranja = fruits.index("Orange")
# print(posicion_naranja)

# SUBLISTAS

numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

de_2_a_6 = numbers[2:7]
# print(de_2_a_6)

mas_de_4 = numbers[5:]

# print(mas_de_4)

menos_de_4 = numbers[:4]
# print(menos_de_4)

# print(numbers[::2])

# print(numbers[1::2])

numbers.append(11)

fruits.insert(2, "Banana")
# print(numbers, fruits)

fruits.sort()  # reverse=True

# print(fruits)

fruits.remove("Banana")

# print("Banana" in fruits, fruits)

# TUPLAS  - Son un tipo de lista no mutable

cars_tuple = ("ford", "fiat", "jeep")

# print(cars_tuple[1])

# DICCIONARIOS - Colección desordenada. Se almacenan con estructura de clave-valor

fighter = {
    "name": "Carlo Pedersoli",
    "alias": "Bud Spencer",
    "country": "Italy"
}

# print(fighter.get("nombre"))

claves = fighter.keys()
valores = fighter.values()

# print(list(claves), list(valores))

print(list(valores)[2])



