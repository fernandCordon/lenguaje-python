
# Un conjunto es una colección desordenada de objetos, que aparecen al menos una vez.

import numpy as np

A = {"Juan", "Luis", "Ana"}
B = {2, 2, 1, 1, 3, 3}
# print(B)

C = {1+2, 3, 'a'}
# print(C)

# print("fernando" in A)
# print(5 not in B)

x = range(10)

# print(list(x))

lista = ["rosa", "rosa", "lirio", "amapola", "violeta"]

D = set(lista)
# print(D)

# 2 ** 0, 2 **1, 2**2
E = {2 ** each for each in range(10)}

# print(E)

lista = np.arange(1, 2, 0.1)
# print(lista)

F = {-3, -2, -1, 0, 1, 2, 3}
subF = {x for x in F if x >= 0}

# print(subF)

I = {1, 'k', 'z', 2, 7}
J = {'z', 1, 7}

J_comp = {x for x in I if x not in J}
print(J_comp)







