from math import sqrt, floor, ceil

# TIPOS Y VARIABLES


entero = 44
real = 12.34
# print(real)
# 0 1 2 3 4 5 6 7 8 9 a b c d e f
hexadecimal = 0x7d

# print(float(hexadecimal))
# print(hex(255))

booleana1 = 7 == 3
booleana2 = 6 > 5
booleana3 = 'Fernando' != 'Fernando'

# print(booleana1, booleana2, booleana3)

# print(type(entero))

saludo = "Hello"

mensaje = "Los 0's nos dan mochos problemas"
# print(mensaje)

# print(mensaje.split())

# OPERACIONES BÁSICAS

a = 8 * 3
b = 24 + 6
c = 30 / 10
d = 10 - 8
e = 7 / 2

# print(a, b, c, d, e)

f = 7 // 2
g = -7 // 2

# print(f, g)

i = 2 ** 3
j = pow(2, 3)

# print(i, j)

k = 27 ** (1/3)
# print(k)

print(16 ** (1/2), sqrt(16))

m = int(4.14655)

o = round(4.14655)
q = round(4.14655, 2)
print(o, q)

bajo = floor(4.14655)
alto = ceil(4.14655)












