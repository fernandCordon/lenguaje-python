
class Perro:

    def __init__(self, nombre, edad, peso):
        self.nombre = nombre
        self.edad = edad
        self.peso = peso

    def ladrar(self):
        if self.peso >= 8:
            print("GUAU, GUAU")
        else:
            print("guau, guau")

    def __str__(self):
        return "Perro {0}, edad: {1}, peso: {2}".format(self.nombre, self.edad, self.peso)


bob = Perro('Bob', 3, 2)

# print(bob)

# bob.ladrar()


# HERENCIA DE CLASES

class PerroAsistencia(Perro):

    __trabajando = False

    def __init__(self, nombre, edad, peso, amo):
        Perro.__init__(self, nombre, edad, peso)
        self.amo = amo

    def pasear(self):
        print("Yo, {0}, acompaño a mi dueño {1} a pasear".format(self.nombre, self.amo))


    def ladrar(self):
        if self.trabajando():
            print("Shhhh, no puedo ladrar, alguien trabaja: {}".format(self.trabajando()))
        else:
            Perro.ladrar(self)



    def trabajando(self, valor=None):
        if valor == None:
            return self.__trabajando
        else:
            self.__trabajando = valor


    def __str__(self):
        return "Perro de asistencia de {}".format(self.amo)


pep = PerroAsistencia("Pep", 2, 7, "Luis")

# print(pep)
# print(pep)
pep.trabajando("Bombero")
pep.ladrar()








