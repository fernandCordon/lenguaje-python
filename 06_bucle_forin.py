
frutas = ["manzana", "naranja", "pera", "mandarina"]

for fruta in frutas:
    print('Me como una {}'.format(fruta))

pronombres = {'yo': 'I', 'tú': 'You', 'el': 'He', 'ella': 'She'}

for clave, valor in pronombres.items():
    print('la Traducción de {0} es {1}'.format(clave, valor))


listaPrecios = [12, 20, 200, 30]
listaUnidades = [1, 8, 1, 5]

for precio, unidades in zip(listaPrecios, listaUnidades):
    resultado = precio * unidades
    print('{0} € - {1} unidades = {2} €'.format(precio, unidades, resultado))



