
import time

def escribeTrasUnaEspera(tiempo, texto, callback):
    print(texto)
    time.sleep(tiempo)
    callback()


# escribeTrasUnaEspera(3, 'HOLA...', lambda: print('Ya he terminado: Adiós'))


def serieNum(n, fn, callbackFin):

    if n == 0:
        callbackFin()
        return

    n -= 1

    fn(1.5, "texto {}".format(n), lambda: serieNum(n, fn, callbackFin))


serieNum(5, escribeTrasUnaEspera, lambda: print("Por fin he acabado"))


