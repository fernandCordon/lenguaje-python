
# Programa que calcula la edad humana de los gatos: Edad gatuna

strEdad = ""

while strEdad == "":
    strEdad = input("Edad del gato:")

    if not strEdad.isdigit():
        print("La edad del gato ha de ser un número entero")
        strEdad = ""

edad = int(strEdad)

edadGatuna = edad * 7

print('Tu gato tiene {} años humanos y {} años gatunos'.format(edadGatuna, edad))



