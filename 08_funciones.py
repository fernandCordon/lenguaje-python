def sumar(numero, otronumero):
    return numero + otronumero


resul = sumar(2345, 623)
# print(resul)

frutas = ["manzana", "naranja", "pera", "mandarina"]
otrasFrutas = ["melón", "sandía", "pera", "piña"]


def imprimeListaFrutas(arr):
    for element in arr:
        if element != "pera":
            print(element)


# imprimeListaFrutas(frutas)


def sumaTodos(lista_numeros):
    suma = 0

    for each in lista_numeros:
        suma += each

    return print(suma)


lista = [3, 5, 8, 2, 0, 6, 12]

# sumaTodos(lista)

# Ámbito de las variables

a = 5
b = 6
c = 15
d = 1


def blackBox(a, b):
    c = a + b + d  # c es local y d es global
    print("valor de: {}".format(d))
    return c


# print(blackBox(3, 1))

def otraBlackBox(a, b):
    global d
    d = d * 2  # d es la global
    c = a + b + d
    # print("valor de d: {}".format(d))
    return c


# print(otraBlackBox(3, 1))


# Argumentos de una función

# Argumentos fijos

def add(s1, s2):
    return s1 + s2


# Argumentos por defecto

def componerPalabra(palabra, sufijo='mente'):
    resultado = palabra + sufijo
    return resultado


# print(componerPalabra('fácil', 'ísimo'))

# print(componerPalabra('mala'))

# Argumentos variables


def media(*numeros):
    suma = 0
    for num in numeros:
        suma += num

    return suma / len(numeros)


# print(media(1, 6, 88, 35, 77, 12, 100))

lista = [2, 5, 7, 1, 4]

# print(media(*lista))


# Excepciones

def estoPuedeFallar(num):
    return 1 / num

try:
    print(estoPuedeFallar(3))
except:
    print("Error: No se uede dividir por 0")




