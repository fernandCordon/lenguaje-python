

def logger(funcion_decorada):

    def wrapper(*args):
        print("Argumentos: {}".format(args))
        return funcion_decorada(*args)

    return wrapper


def sumar(a, b, c, d):
    print(a + b + c + d)


decorated_fn = logger(sumar)

# decorated_fn(4, 8, 6, 3)


@logger
def restar(num, menos):
    print(num - menos)


# restar(8, 5)

@logger
def saludar(nombre):
    print("Hola {}".format(nombre))

saludar("Fernando")