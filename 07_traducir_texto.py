
# print(ord('A'))

# print(hex(111))
# 0 1 2 3 4 5 6 7 8 9 a b c d e f

cadena = ""

while cadena == "":
    cadena = input("Encriptación hexadecimal de lo que aquí escribas:")

    traduccion = ""

    for caracter in cadena:
        unicode = ord(caracter)
        valorHexadecimal = hex(unicode)
        segundoPar = valorHexadecimal[2:4]
        valor = segundoPar if caracter != ' ' else ' '
        traduccion += "{}".format(valor)

    print(traduccion)
